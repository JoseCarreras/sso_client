<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Mail;
use App\Mail\RecoverPassword;

class MailTest extends TestCase
{
    public function test_send_email()
    {
        $email = 'julio.perezdecastro@vanadis.es';
        Mail::fake();

        Mail::to($email)->send(new RecoverPassword());

        Mail::assertSent(RecoverPassword::class, 1);
    }
}
