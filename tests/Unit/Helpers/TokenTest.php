<?php

namespace Tests\Unit\Helpers;

use PHPUnit\Framework\TestCase;
use App\Helpers\Token;


class TokenTest extends TestCase
{

    public function test_add_data_and_return_correct_token()
    {
        $data = [
            'email' => 'joe@doe.com'
        ];
        $return_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImpvZUBkb2UuY29tIn0.yYqpHxWB9Oip-DAK4Knsub24YGOgYqrpMF7RDfDOi1o";

        $token = new Token($data);

        $this->assertEquals($token->encode(), $return_token);
    }

    public function test_encode_token_without_data_thow_exception()
    {
        $token = new Token();
        $message_exception = 'when data is null cant encode token';

        try
        {
            $token->encode();
        }
        catch (\Throwable $th)
        {
            $this->assertEquals($th->getMessage(), $message_exception);
        }
    }

    public function test_when_token_not_correct_to_decode_thow_exception()
    {
        $token = new Token();
        $message_exception = 'Wrong number of segments';

        try
        {
            $token->decode('lkñasdf');
        }
        catch (\Throwable $th)
        {
            $this->assertEquals($th->getMessage(), $message_exception);
        }
    }

    public function test_when_token_is_correct_can_decode_with_correct_data()
    {
        $token = new Token();
        $token_correct = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImpvZUBkb2UuY29tIn0.yYqpHxWB9Oip-DAK4Knsub24YGOgYqrpMF7RDfDOi1o";
        $data_correct = new \stdClass();
        $data_correct->email = 'joe@doe.com';

        $token_decode = $token->decode($token_correct);

        $this->assertEquals($token_decode, $data_correct);
    }
}
