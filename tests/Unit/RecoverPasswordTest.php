<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\Mail\RecoverPassword;
use App\Helpers\PasswordGenerator;

class RecoverPasswordTest extends TestCase
{

    public function test_user_recover_password_template_when_send_email_recover_password()
    {
        $password_generator = new PasswordGenerator();
        $user = new User();
        $user->password = $password_generator->get_password_hashed();

        $recover_password_template_email = new RecoverPassword($password_generator);
        $html = $recover_password_template_email->build();

        $this->assertEquals($html->view, 'emails.recover_password');
    }

    public function test_view_password_text_plain_when_generate_email_template()
    {
        $password_generator = new PasswordGenerator();
        $user = new User();
        $user->password = $password_generator->get_password_hashed();

        $recover_password_template_email = new RecoverPassword($password_generator);
        $html = $recover_password_template_email->build();

        $this->assertEquals($html->viewData['password'], $password_generator->get_password_text_plain());
    }
}
