<?php
namespace App\Http\Controllers;


use App\User;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Token;
use App\Helpers\Passwords;
use Illuminate\Support\Str;
use App\Helpers\PasswordGenerator;
use App\Mail\RecoverPassword;
use Illuminate\Support\Facades\Mail;
use Cviebrock\DiscoursePHP\SSOHelper;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

use GuzzleHttp\Client;

class LogoutController extends Controller
{
    protected $headers = [
        'headers' => [
            'Api-Key'  => "5db57d0b3a00a154e90648af361e76c81973bef6265c55c37a166c809e6016e6",
            'Api-Username'  => 'system'
        ]
    ];

    protected $url = 'https://cevappdiscourse.dev.vanadis.es';
 
    /**
     * Gets the user Id by making a request to Discourse API
     */
    public function get_user_id(){
        $client = new Client();

        if (isset($_COOKIE['email'])) {

            $response = $client->request('GET', $this->url . '/u/by-external/'. $_COOKIE['email'] .'.json', $this->headers);

            if ($response->getStatusCode() == 200) { // 200 OK

                //Get data from request
                $response_data = $response->getBody()->getContents();

                $data = json_decode($response_data, true);

                //gets user id from data
                $user_id = $data['user']['id'];
    
                return $user_id;
            }else{
                return $res->getStatusCode();
            }
        }            
    }

    public function logout(Request $request)
    {
        if (isset($_COOKIE['email'])) {
            $client = new Client();
            
            //Send request to log the user out in Discourse
            $response = $client->request('POST', $this->url . '/admin/users/' . $this->get_user_id() .'/log_out.json', $this->headers);

            //Delete user email from cookies
            unset($_COOKIE['email']); 
            setcookie('email', null, -1, '/');
            return Redirect::to('http://discourse.vanadis.me/login');

        } else {
            return Redirect::to('http://discourse.vanadis.me/login');
        }
    }
}