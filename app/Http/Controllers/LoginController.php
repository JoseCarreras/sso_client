<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Token;
use App\Helpers\Passwords;
use Illuminate\Support\Str;
use App\Helpers\PasswordGenerator;
use App\Mail\RecoverPassword;
use Illuminate\Support\Facades\Mail;
use Cviebrock\DiscoursePHP\SSOHelper;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Http;

class LoginController extends Controller
{
    
    public function login(Request $request)
    {
        try {
            $user = User::where('email', $request->email)->first();
            if ($user == NULL)
            {
                return response()->json([
                    'message' => 'Wrong user or password',
                ], 401);
            }

            if (Hash::check($request->password, $user->password))
            {
                $data_token = ['email' => $user->email];
                $token_builder = new Token($data_token);
                $token = $token_builder->encode();

                setcookie("email", $user->email, time()+3600);
                return Redirect::to('http://discourse.vanadis.me/discourse');
            }
            else
            {
                return response()->json([
                    'message' => 'Wrong user or password',
                ], 401);
            }
        } catch (\Throwable $th) {
            var_dump($th->getMessage()); exit;
        }
    }


    public function login_discourse(Request $request){

        $sso = new SSOHelper;

        // this should be the same in your code and in your Discourse settings:
        $secret = 'hseefa5rgjuj81asyjk';
        $sso->setSecret( $secret );

        // load the payload passed in by Discourse
        $payload = $_GET['sso'];
        $signature = $_GET['sig'];
        
        // validate the payload
        if (!($sso->validatePayload($payload,$signature))) {
            // invaild, deny
            header("HTTP/1.1 403 Forbidden");
            echo("Bad SSO request");
            die();
        }
        
        $nonce = $sso->getNonce($payload);
        
        try {
            
            $email = $_COOKIE["email"];
            /*
            $user = User::where('email', $email)->first();
            
            if ($user == NULL)
            {
                return response()->json([
                    'message' => 'Wrong user or password',
                ], 401);
            }
            */
        } catch (\Throwable $th) {
            var_dump($th->getMessage()); exit;
        }

        // Required and must be unique to your application
        $userId = $email;

        // Required and must be consistent with your application
        $userEmail = $email;
        
        // build query string and redirect back to the Discourse site
        $query = $sso->getSignInString($nonce, $userId, $userEmail);
        
        try{
            return Redirect::to('https://cevappdiscourse.dev.vanadis.es/session/sso_login?'. $query);
        }catch (\Throwable $th) {
            var_dump($th->getMessage()); exit;
        }
        
    }

    
    public function recover_password(Request $request)
    {
        if ($request->email == '')
        {
            return response()->json([
                'message' => 'Missing parameters',
            ], 400);
        }

        $user = User::where('email', $request->email)->first();

        if ($user == NULL)
        {
            return response()->json([
                'message' => 'User not found',
            ], 404);
        }

        $password_generator = new PasswordGenerator();
        $user->password = $password_generator->get_password_hashed();
        $user->save();

        Mail::to($user->email)->send(new RecoverPassword($password_generator));

        return response()->json([
            'message' => 'Password sent',
        ], 200);
    }
}
