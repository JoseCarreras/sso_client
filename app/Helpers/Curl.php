<?php

namespace App\Helpers;

class Curl
{
    private $url;
    private $email;

    public function __construct($params = [])
    {
        if ($params == [])
        {
            throw new Exception('wrong set data in curl request');
        }

        $this->url = $params['url'];
        $this->email = $params['email'];

    }

    public function post()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $this->email,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $error = curl_error($curl);
        $data = [
            'response' => $response,
            'error' => $error
        ];

        curl_close($curl);

        var_dump($data);exit;
        return $data;
    }
}
