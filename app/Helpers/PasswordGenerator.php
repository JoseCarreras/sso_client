<?php

namespace App\Helpers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class PasswordGenerator
{
    private $password;
    private $password_hashed;

    public function __construct()
    {
        $this->password = Str::random(8);
        $this->password_hashed = Hash::make($this->password);
    }

    public function get_password_text_plain()
    {
        return $this->password;
    }

    public function get_password_hashed()
    {
        return $this->password_hashed;
    }
}
